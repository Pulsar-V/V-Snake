#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QPushButton"
#include "QVBoxLayout"
#include "QMessageBox"
#include "QPainter"
#include "QTime"
#include "QPoint"
#include "iostream"
#define SQURE 10
#define WIDTH 640
#define HEIGHT 480
#define STEP 5
#define SPEED 50
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    is_start=false;
    ui->setupUi(this);
    setFixedSize(WIDTH,HEIGHT);
    start= new QPushButton(tr("start"));
    start->setGeometry(0,0,20,40);
    vbox = new QVBoxLayout(this);
    vbox->addWidget(start);
    ui->centralWidget->setLayout(vbox);
    connect( start, SIGNAL(clicked()), this, SLOT(slotClicked()));
    this->is_eat=true;
    this->snake=new Snake;
    this->snake->is_head=1;
    this->snake->next=NULL;
    this->snake->pre=NULL;
    this->snake->color=0;
    //createSnake(this->snake,4);
    main_timer=new QTimer;
    connect(main_timer,SIGNAL(timeout()),this,SLOT(update()));
    //connect(main_timer,SIGNAL(timeout()),this,SLOT(dosomethings()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::slotClicked()
{
    this->start->setVisible(false);
    is_start=true;
    this->main_timer->start(SPEED);
}
void  MainWindow::dosomethings()
{
    //QMessageBox::about( this, "Mouse Example", "You have pressed mouse, exit now!");

}
void MainWindow::paintEvent(QPaintEvent *event){
    if(this->is_start==true){
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing, true);
        painter.setViewport(0,0,WIDTH,HEIGHT);
        painter.setWindow(0, 0, WIDTH, HEIGHT);
        drawWorld(&painter);
        createFood(&painter);
        eatFood();
        moveSnake();
        drawSnake(&painter,this->snake);
        outWorld(this->snake);
        std::cout<<"Snake->"<<snake->x<<","<<snake->y
                <<"  Food->"<<this->food->x<<","<<food->y
               <<"  Len "<<getSnakeLength(this->snake)
              <<"is head"<<this->snake->is_head<<std::endl;
    }
    //std::cout<<"Paint"<<std::endl;
}
void MainWindow::eatFood(){
    if(this->snake->x+SQURE/2>this->food->x&&
            this->snake->y+SQURE/2>this->food->y&&
            this->snake->x+SQURE/2<this->food->x2&&
            this->snake->y+SQURE/2<this->food->y2){
        //free(this->food);
        addSnake(this->snake);
        this->is_eat=true;
    }
}

void MainWindow::moveSnake(){
    if(this->snake->pos==1)
        this->snake->y-=STEP;
    if(this->snake->pos==2)
        this->snake->y+=STEP;
    if(this->snake->pos==3)
        this->snake->x-=STEP;
    if(this->snake->pos==4)
        this->snake->x+=STEP;
    Snake *pt = this->snake->next;
    Snake *temp=this->snake;

    while (pt != NULL) {
        pt->x=temp->h_x;
        pt->y=temp->h_y;
        temp->h_x=temp->x;
        temp->h_y=temp->y;
        qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
        pt->color=qrand()%(19-6);
        temp=pt;
        pt = pt->next;
    }
}

void MainWindow::outWorld(Snake *head){
    if(head->x>WIDTH)
        head->x=0;
    if(head->x<0)
        head->x=WIDTH;
    if(head->y>HEIGHT)
        head->y=0;
    if(head->y<0)
        head->y=HEIGHT;
}

void MainWindow::keyPressEvent(QKeyEvent *event){
    //w 87 a65 s83 d68
    int key=(int)event->key();
    this->snake->h_x=this->snake->x;
    this->snake->h_y=this->snake->y;
    Snake *pt = this->snake->next;
    if(pt!=NULL)
        this->snake->h_pos=this->snake->pos;
    while (pt != NULL) {
        pt->h_x=pt->x;
        pt->h_y=pt->y;

        pt=pt->next;
    }
    switch (key) {
    case 87:
        this->snake->pos=1;
        //std::cout<<"up"<<std::endl;
        break;
    case 83:
        this->snake->pos=2;
        //std::cout<<"down"<<std::endl;
        break;
    case 65:
        this->snake->pos=3;
        //std::cout<<"left"<<std::endl;
        break;
    case 68:
        this->snake->pos=4;
        //std::cout<<"right"<<std::endl;
        break;
    default:
        break;
    }

}

void MainWindow::drawWorld(QPainter *painter){
    painter->translate(0,0);//重新设定坐标原点
    painter->setBrush(Qt::white);
    painter->setPen(Qt::black);
    painter->save();//保存坐标系，防止坐标系跑偏了
    painter->drawRect(0,0,WIDTH,HEIGHT);//绘制
    painter->restore();//绘制图形后复位坐标系
}
void MainWindow::createFood(QPainter *painter){
    if(this->is_eat==true){
        this->food=new Food;
        qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
        this->food->x=qrand()%(600-20);
        this->food->y=qrand()%(400-20);
        this->food->x2=food->x+SQURE;
        this->food->y2=food->y+SQURE;
        painter->translate(0,0);//重新设定坐标原点
        painter->setBrush(Qt::black);
        painter->setPen(Qt::black);
        painter->save();//保存坐标系，防止坐标系跑偏了
        painter->drawRect(food->x,food->y,SQURE,SQURE);//绘制
        painter->restore();//绘制图形后复位坐标系
        this->is_eat=false;
    }else {
        painter->translate(0,0);//重新设定坐标原点
        painter->setBrush(Qt::black);
        painter->setPen(Qt::black);
        painter->save();//保存坐标系，防止坐标系跑偏了
        painter->drawRect(food->x,food->y,SQURE,SQURE);//绘制
        painter->restore();//绘制图形后复位坐标系
    }
}
void MainWindow::addSnake(Snake *head){
    Snake *pt = NULL, *p_new = NULL;
    int pos;
    pos=getSnakeLength(head);
    p_new = new Snake;
    while (1) {
        pos--;
        if (0 == pos)
            break;
        head = head->next;
    }

    pt = head->next;
    //p_new->pos=head->pos;
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    p_new->color=qrand()%(19-6);
    p_new->next = pt;
    if (NULL != pt)
        pt->pre = head;
    p_new->pre = head;
    head->next = p_new;
}

void MainWindow::drawSnake(QPainter *painter, Snake *head){
    painter->translate(0,0);//重新设定坐标原点
    painter->save();//保存坐标系，防止坐标系跑偏了
    painter->setBrush(Qt::color0);
    painter->setPen(Qt::red);
    painter->drawRect(head->x,head->y,SQURE,SQURE);//绘制
    Snake *pt = head->next;
    while (pt != NULL) {
        painter->setPen(pt->color);
        painter->drawRect(pt->x,pt->y,SQURE,SQURE);//绘制
        //std::cout<<"is head ->"<<pt->is_head<<std::endl;
        pt = pt->next;
    }

    painter->restore();//绘制图形后复位坐标系
}
int MainWindow::getSnakeLength(Snake *head){
    int length = 1;
    Snake *pt = head->next;
    while (pt != NULL) {
        length++;
        pt = pt->next;
    }
    return length;
}
