#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QPushButton"
#include "QVBoxLayout"
#include "QMessageBox"
#include "QTimer"
#include "QPoint"
#include "QPainter"
#include "QKeyEvent"
namespace Ui {
class MainWindow;

}
struct Snake{
    int color=7,x=200,y=330,h_x=200,h_y=300,h_pos=4,is_head=0,pos=4;
    struct Snake *next;
    struct Snake *pre;
};
struct Food{
    int x,y,x2,y2;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();
signals:
   void clicked();
   void timeout();
private slots:
   void slotClicked();
   void dosomethings();
private:
   QPushButton *start;
   QVBoxLayout *vbox;
   Ui::MainWindow *ui;
   QTimer *main_timer;
   struct Snake *snake;
   struct Food *food;
   bool is_start;
   bool is_eat;
protected:
   void paintEvent(QPaintEvent *event);
   void drawWorld(QPainter *painter);
   void drawSnake(QPainter *painter,Snake *head);
   void createSnake(Snake *head,int init_length);
   int getSnakeLength(Snake *head);
   void addSnake(Snake *head);
   int isSnakeEmpity(Snake *head);
   void freeMemory(Snake *head);
   void keyPressEvent(QKeyEvent *event);
   void outWorld(Snake *head);
   void moveSnake();
   void createFood(QPainter *painter);
   void eatFood();
};

#endif // MAINWINDOW_H
